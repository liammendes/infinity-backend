<!DOCTYPE html>
<!-- [START_EXCLUDE] -->
<%--
  ~ Copyright 2017 Google Inc.
  ~
  ~ Licensed under the Apache License, Version 2.0 (the "License"); you
  ~ may not use this file except in compliance with the License. You may
  ~ obtain a copy of the License at
  ~
  ~     http://www.apache.org/licenses/LICENSE-2.0
  ~
  ~ Unless required by applicable law or agreed to in writing, software
  ~ distributed under the License is distributed on an "AS IS" BASIS,
  ~ WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
  ~ implied. See the License for the specific language governing
  ~ permissions and limitations under the License.
  --%>
<!-- [END_EXCLUDE] -->
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
  <title>Infinity API</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="https://code.getmdl.io/1.3.0/material.blue-deep_purple.min.css" />
    <link rel="stylesheet" href="infinity.css"/>
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro" rel="stylesheet">
  <script defer src="https://code.getmdl.io/1.3.0/material.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.6.2/css/bulma.min.css">
    <script defer src="https://use.fontawesome.com/releases/v5.0.0/js/all.js"></script>
</head>
<body>
<!-- Always shows a header, even in smaller screens. -->
<div class="mdl-layout mdl-js-layout mdl-layout--fixed-header">
    <header class="mdl-layout__header">
        <!-- Infinity Icon -->
        <!--<div class="mdl-layout-icon">
            <svg xmlns="http://www.w3.org/2000/svg" class="infinity-icon" viewBox="0 0 1024.29 1024.29"><defs><style>.cls-1{fill:#fff;}</style></defs><title>Infinity</title><g id="Layer_1" data-name="Layer 1"><path class="cls-1" d="M511.85,0C229,0-.29,229.3-.29,512.15A510,510,0,0,0,108.69,828c93.76,119.5,239.49,196.27,403.16,196.27C794.7,1024.29,1024,795,1024,512.15S794.7,0,511.85,0ZM50.09,529.85c-.25-354.45,924.18-354.45,923.52,0C973.61,1070.57,50.09,1058.43,50.09,529.85Z"/></g></svg>
        </div>-->
        <div class="mdl-layout__header-row">
            <!-- Title -->
            <span class="mdl-layout-title">Infinity API</span>
        </div>
    </header>
    <main class="mdl-layout__content">
        <div class="page-content">
            <h3 class="header-text">Build. Great. Things.</h3>
            <p class="content">The Infinity App uses the Infinity API to carry out certain actions on behalf of the user. Below you can find instructions, links to documentation and information about using the Infinity SDK.</p>
        </div>
    </main>
</div>
</body>
</html>

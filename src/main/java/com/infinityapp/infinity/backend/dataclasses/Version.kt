package com.infinityapp.infinity.backend.dataclasses

import com.infinityapp.infinity.backend.utils.Variables

class Version {

    var success = 1
    var response = Variables.SUCCESS_RESPONSE
    var version = "API@Infinity 1.0.0"

}